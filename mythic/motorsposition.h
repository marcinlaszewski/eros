/*
 * motorspoistion.h
 *
 *  Created on: 30 sept. 2015
 *      Author: falexandre
 */

#ifndef 	MOTORPOSITION_H_
#define 	MOTORPOSITION_H_

typedef union
{
    unsigned short statut;
    struct {
        unsigned short  H:1;
        unsigned short  BUSY:1;
        unsigned short  SW_F:1;
        unsigned short  SW_EN:1; // The SW_EVN flag is active high and indicates a switch turn-on event (SW input falling  edge).
        unsigned short  DIR:1;
        unsigned short  MOTSTATUT:2;
        // The NOTPERF_CMD and WRONG_CMD flags are active high and indicate,
        // respectively, that the command received by SPI can't be performed or does not exist at all. The SW_F
        // reports the SW input status (low for open and high for closed).
        unsigned short  NOTPERF_CMD:1;      //
        unsigned short  WRONG_CMD:1;
        //        The UVLO, TH_WRN, TH_SD, OCD, NOTPERF_CMD, WRONG_CMD and SW_EVN flags
        //        are latched: when the respective conditions make them active (low or high) they remain in
        //        that state until a GetStatus command is sent to the IC.
        unsigned short  UVLO :1;
        unsigned short  TH_WRN :1;
        unsigned short  TH_SD :1;
        unsigned short  OCD :1;
        unsigned short  xx :2;
        unsigned short  SCK_MOD :1;
    } ;
    unsigned char cstat[2];

} motorstatutregister ;



enum
{
    C_HOME='0',//48
    C_RBC_CHAMBER_POSITION,
    C_RBC_HGB_CHAMBER_POSITION,
    C_BETWEEN_CHAMBER_POSITION,
    C_WBC_CHAMBER_POSITION,
    C_OT_POSITION,
    C_CT_POSITION,
    C_MIXER_POSITION_5,
    C_MIXER_POSITION_4,
    C_MIXER_POSITION_3,
    C_MIXER_POSITION_2,
    C_MIXER_POSITION_1,
    C_RACK_ID,
    C_RINCE_RBC_CHAMBER_POSITION,
    C_RINCE_WBC_CHAMBER_POSITION,
    C_DEC_RETURN_HOME,
    C_POSITION_CHAMBER_MYTHIC_80,
    C_POSITION_FRONT_5_MYTHIC_80,
    C_POSITION_FRONT_4_MYTHIC_80,
    C_POSITION_FRONT_RACK_ID_MYTHIC_80,
    C_POSITION_FRONT_3_MYTHIC_80,
    C_POSITION_FRONT_2_MYTHIC_80,
    C_POSITION_FRONT_1_MYTHIC_80,
    C_POSITION_BACK_5_MYTHIC_80,
    C_POSITION_BACK_4_MYTHIC_80,
    C_POSITION_BACK_RACK_ID_MYTHIC_80,
    C_POSITION_BACK_3_MYTHIC_80,
    C_POSITION_BACK_2_MYTHIC_80,
    C_POSITION_BACK_1_MYTHIC_80,
    C_OPEN_TUBE_MYTHIC_80,//77

};

enum
{
    T_HOME='0',
    T_POS_LOAD_IN_MIXER,

    T_TEST_PRESTUB_1,
    T_TEST_PRESTUB_2,
    T_TEST_PRESTUB_3,
    T_TEST_PRESTUB_4,
    T_TEST_PRESTUB_5,

    T_POS_UNLOAD_IN_MIXER,
    T_POS_UNLOADED,

    T_UNLOAD_CHECK_SWITCH_POS,
    T_UNLOAD_CHECK_SWITCH_POS_FAST,
    T_UNLOAD_RACK_NO_TUBE,
    T_UNLOAD_CHECK_SWITCH_HOMELOAD_CHECK,
    T_UNLOAD_CHECK_SWITCH_MIXERLOAD_CHECK,

    T_HIGH_SPEED_PREPOSITION_RETURN,

    T_INIT_UNLOAD_TRANSFER_POS,
    T_POS_INIT_UNLOAD_RACK_TRANSFER_POS,

};

enum
{
    N_HOME = '0',
    N_BEFORE_SAMPLING,          //N_AVANT_PERCAGE,
    N_AFTER_SAMPLING,           //N_APRES_PERCAGE,
    N_PRELEV_TUBE_TYPE_1,       //OBSOLETE
    N_PRELEV_TUBE_TYPE_2,       //OBSOLETE
    N_PRELEV_TUBE_TYPE_3,       //OBSOLETE
    N_SAMPLING_NORMAL_TUBE,     // N_SAMPLING_NORMAL_TUBE,
    N_SAMPLING_MAP_TUBE,        // N_SAMPLING_MAP_TUBE,
    N_SAMPLING_SARSTEDT_TUBE,   // N_PRELEV_TF_SARSTEDT,
    N_HEAD_DISTRI_TUBE,         // N_DISTRI_TETE,
    N_BEFORE_RINCE_NORMAL_TUBE, // N_RETOUR_AVANT_RINCAGE_TF_NORMAL,
    N_BEFORE_RINCE_MAP_TUBE,    // N_RETOUR_AVANT_RINCAGE_TF_MAP,
    N_BEFORE_RINCE_SARSTETD_TUBE,//    N_RETOUR_AVANT_RINCAGE_TF_SARSTETD,
    N_3UL_REJECTION,            //    N_REJET_3UL,
    N_NEEDLE_MASK,              //    N_MASQUE_AIGUILLE,
    N_NEEDLE_DRY,               //    N_SECHE_AIGUILLE,
    N_NEEDLE_PRIME,             //    N_AMORCAGE_AIGUILLE,
    N_NEEDLE_DRY_HEAD,          //    N_SECHE_TETE,
    N_SAMPLING_OT,              //    N_PRELEV_OT_NORMAL,
    N_CLEAN_CHAMBER,        //    N_POSITION_CLEAN_BAC,

};

enum
{
    S_HOME = '0',
    S_PRELEVEMENT,
    S_POUSSE_3UL,
    S_DISTRI_GR,
    S_DISTRI_GB,
    S_DISTRI_HGB,

};

enum
{
    D_HOME  = '0',
    D_CHARGEMENT,
    D_POS_1,
    D_POS_2,
    D_POS_3,
    D_POS_4,
    D_DECHARG_TOT,
    D_DECHARG_HGB,

};

enum
{
    M_HOME,
    M_MIXER_POS_1,
    M_MIXER_POS_2,
    M_MIXER_POS_3,
    M_MIXER_POS_4,
    M_MIXER_POS_5,
    M_MIXER_POS_6,
    M_MIXER_POS_7,
    M_MIXER_POS_8,

};

#endif
