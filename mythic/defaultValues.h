/*
 * defaultValues.h
 *
 *  Created on: 11 mars. 2015
 *      Author: falexandre
 */

#ifndef 	DEFAULTVALUES_H_
#define 	DEFAULTVALUES_H_

/*Needle adjustments*/
/// value for productions settings, this value is intend to prepositionnized the needle to enable a good setting
#define     NEEDLE_REFERENCE_INIT_POS_DISTRI         1950

/// mecanical references
#define     NEEDLE_DISTRI_REFERENCE_UM_POS          21150//21150//21300    /*µm = 21.3mm*/  /*tete noire*/
///                                                 21700    /*µm = 21.4mm*/    /*tete transparente*/
#define     STEP_MODE_COEF                          4
#define     NEEDLE_MASK_REFERENCE_UM_POS                 18750    /*µm = 18.75mm*/
#define     CONV_STEP_TO_UM                      150     /*1 step = 150µm*/
#define     CONV_NM_TO_STEP                      9375    /*nm -> 1/16 step= 9.375 µm*/

#define     NEEDLE_DISTRI_REFERENCE_1_16_STEP_POS      ((NEEDLE_DISTRI_REFERENCE_UM_POS/CONV_STEP_TO_UM) << STEP_MODE_COEF)//2272
#define     NEEDLE_MASK_REFERENCE_1_16_STEP            ((NEEDLE_MASK_REFERENCE_UM_POS/CONV_STEP_TO_UM) << STEP_MODE_COEF)

#define     RES_GAIN            181*16
#define     OPT_GAIN            11*11

#define     GAIN_AND_THRES_DCOUT_TARGET             7000
#define     GAIN_AND_THRES_DCOUT_RETIC_TARGET       11000
#define     GAIN_AND_THRES_DCOUT_EEPOT              4950
#define     GAIN_AND_THRES_DCOUT_RETIC_EEPOT        4950

#define     GAIN_AND_THRES_5DIFF_RES_HIGH_THRES     800
#define     GAIN_AND_THRES_5DIFF_RES_LOW_THRES      800
#define     GAIN_AND_THRES_5DIFF_OPT_THRES          800
#define     GAIN_AND_THRES_5DIFF_RES_GAIN_EEPOT     9000
#define     GAIN_AND_THRES_5DIFF_OPT_GAIN_EEPOT     15000
#define     GAIN_AND_THRES_5DIFF_RES_GAIN_TARGET    0       // to be defined
#define     GAIN_AND_THRES_5DIFF_OPT_GAIN_TARGET    0       // to be defined

#define     GAIN_AND_THRES_3DIFF_RES_GAIN_TARGET    0       // to be defined
#define     GAIN_AND_THRES_3DIFF_OPT_GAIN_TARGET    0       // to be defined
#define     GAIN_AND_THRES_3DIFF_RES_HIGH_THRES     800
#define     GAIN_AND_THRES_3DIFF_RES_LOW_THRES      800
#define     GAIN_AND_THRES_3DIFF_OPT_THRES          800
#define     GAIN_AND_THRES_3DIFF_RES_GAIN_EEPOT     16000
#define     GAIN_AND_THRES_3DIFF_OPT_GAIN_EEPOT     7500

#define     GAIN_AND_THRES_RETIC_RES_HIGH_THRES     800
#define     GAIN_AND_THRES_RETIC_RES_LOW_THRES      800
#define     GAIN_AND_THRES_RETIC_OPT_THRES          0
#define     GAIN_AND_THRES_RETIC_RES_GAIN_EEPOT     9000
#define     GAIN_AND_THRES_RETIC_OPT_GAIN_EEPOT     15000
#define     GAIN_AND_THRES_RETIC_RES_GAIN_TARGET    0       // to be defined
#define     GAIN_AND_THRES_RETIC_OPT_GAIN_TARGET    0       // to be defined

#define     GAIN_AND_THRES_RETIC_TRANSFER_TIME          20
#define     GAIN_AND_THRES_RETIC_TRANSFER_DELTA_TIME    1

#define     GAIN_AND_THRES_HGB_EEPOT                5970
#define     DIL_NEEDLE_INIT                         200
#define     GAIN_AND_THRES_RBC_PLT_RES_HIGH_THRES   1800
#define     GAIN_AND_THRES_RBC_PLT_RES_LOW_THRES    300
#define     GAIN_AND_THRES_RBC_TARGET               0       // to be defined
#define     GAIN_AND_THRES_RBC_EEPOT                19000

#define     GAIN_AND_THRES_HGB_TARGET               0       // to be defined

/*************************************************************************/
#define     STARTUP_TIME                    193260
#define     PRIMINGAFTER_STARTUP            223222
#define     SHUTDOWN_TIME                   120214
#define     PRIMINGLYSE_HGB                 85501
#define     PRIMINGLYSE_5D                  52806
#define     PRIMING_CLEANER                 78901
#define     PRIMING_DILUENT                 124751
#define     PRIMING_ALL                     287240
#define     UNPRIMING_LYSE_HGB              78407
#define     UNPRIMING_LYSE_5D               49924
#define     UNPRIMING_CLEANER               76672
#define     UNPRIMING_DILUENT               83097
#define     UNPRIMING_ALL                   287240

#define     CLEANING_TIME                   57714
#define     FULL_CLEANING_TIME              216903
#define     BLEACH_CLEANING_TIME            288503

#define     DEFAULT_CYCLETUBE_TIME          43030

/**************************************************************************/
#define     ATL_RACK_SWITCH_DISTANCE             6*4+6
#define     ATL_RECOVERY_SENSOR_VALUE            5
/**************************************************************************/
#define     LED_DRIVE_CURRENT                   15
#define     PROXIMITY_PERIOD_SET                1
#define     PULSE_NUMBER_SELECT                 15
#define     PROXIMITY_THRESHOLD                 39

#define     BLUE_CURRENT                        15
#define     RED_CURRENT                         15
#define     GREEN_CURRENT                       15

#define     MANUAL_TIMING                       1400
#define     INTEGRATE_TIME_SET                  0

#define     DC_MODE_DEFAULT                     1// BOOL
#define     _1_10_DEFAULT                       0

#define     RGB_DEFAULT                         1
#define     GAIN_DEFAULT                        1
/**************************************************************************/


#define     DEFAULT_POS_POSITION_BAC_GR             2478
#define     DEFAULT_POS_POSITION_BAC_HGB            2460
#define     DEFAULT_POS_ENTRE_BAC_GR_GB             2763
#define     DEFAULT_POS_BAC_GB                      2781
#define     DEFAULT_POS_TO                          1935
#define     DEFAULT_POS_TF                          1219
#define     DEFAULT_POS_MIXER_POSITION_5            55
#define     DEFAULT_POS_MIXER_POSITION_4            182
#define     DEFAULT_POS_MIXER_POSITION_3            317
#define     DEFAULT_POS_MIXER_POSITION_2            446
#define     DEFAULT_POS_MIXER_POSITION_1            583
#define     DEFAULT_POS_RACK_ID                     243
#define     DEFAULT_POS_RINCE_BAC_GR                2751
#define     DEFAULT_POS_RINCE_BAC_GB                3065
#define     DEFAULT_POS_DEC_RETURN_HOME                 50
#define     DEFAULT_POS_POSITION_CHAMBER_MYTHIC_80      2358
#define     DEFAULT_POS_POSITION_1_5_MYTHIC_80          1831
#define     DEFAULT_POS_POSITION_1_4_MYTHIC_80          1700
#define     DEFAULT_POS_POSITION_RACK_ID_1_MYTHIC_80    1640
#define     DEFAULT_POS_POSITION_1_3_MYTHIC_80          1580
#define     DEFAULT_POS_POSITION_1_2_MYTHIC_80          1450
#define     DEFAULT_POS_POSITION_1_1_MYTHIC_80          1320
#define     DEFAULT_POS_POSITION_2_5_MYTHIC_80          846
#define     DEFAULT_POS_POSITION_2_4_MYTHIC_80          716
#define     DEFAULT_POS_POSITION_RACK_ID_2_MYTHIC_80    656
#define     DEFAULT_POS_POSITION_2_3_MYTHIC_80          596
#define     DEFAULT_POS_POSITION_2_2_MYTHIC_80          466
#define     DEFAULT_POS_POSITION_2_1_MYTHIC_80          336
#define     DEFAULT_POS_OPEN_TUBE_MYTHIC_80             2358

#define OFFSET_PRESSURE_TRANSFER_WBC                    408


#endif
