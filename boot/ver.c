#include <unistd.h>

#include <boot.h>
#include <eros.h>

static unsigned char req_ver[] =
{
  0x01, 0x10, 0x01, 0x21, 0x10, 0x10, 0x04
};
#define	req_LEN	sizeof(req_ver)
#define	rsp_LEN	3

int boot_ver(int f)
{
  ssize_t i;

  i = (write)(f, req_ver, req_LEN);
  if(i < 0)
  {
    perror("write()");
    return -1;
  }

  if(i != req_LEN)
  {
    fputs("boot version request too short.\n", stderr);
    return -2;
  }

  {
    unsigned char buf[1024];

    i = boot_read(f, buf, sizeof(buf), 5);
    if(i != rsp_LEN)
    {
      fprintf(stderr, "boot-ver: wrong response length (3): %d\n", (int)i);
      return -3;
    }

    if(buf[0] != boot_VER)
    {
      fprintf(stderr, "boot-ver: wrong response code (%u): %d\n",
        boot_VER, (int)i);
      return -3;
    }

    return (((unsigned)buf[1]) << 8) | buf[2];
  }
}
