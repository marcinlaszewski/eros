#include <unistd.h>

#include <eros.h>

void boot_enable(unsigned board)
{
  gpio_value(board_GPIO[board],	0);
  gpio_value(board_BUS[board],	0);
  usleep(10000);
  gpio_value(board_BUS[board],	1);
  usleep(1200);
}
