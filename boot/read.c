#include <unistd.h>

#include <boot.h>
#include <eros.h>

static int read_c(int f, unsigned char * c, struct timeval * tv)
{
  int i;

  {
    fd_set fr;

    FD_ZERO(&fr);
    FD_SET(f, &fr);

    i = select(f + 1, &fr, NULL, NULL, tv);
  }

  if(i < 0)
    return -1;

  if(i)
  {
    i = (read)(f, c, 1);
    if(i <= 0)
      return -2;

    return 1;
  }

  return 0;
}

int boot_read(int f, unsigned char * buf, size_t len, unsigned timeout)
{
  struct timeval	tv = { timeout, 0 };
  ssize_t		i;

  /***
  if(!n)
    return -5;
  */

  for(;;)
  {
    i = read_c(f, buf, &tv);
    if(i <= 0)
      return i;

    if(*buf == DLE)
    {
      i = read_c(f, buf, &tv);
      if(i <= 0)
        return i;
    }
    else if(*buf == SOH)
      break;
  }

  {
    size_t n;

    for(n = 0; n < len;)
    {
      i = read_c(f, buf + n, &tv);
      if(i <= 0)
        return i;

      if(buf[n] == DLE)
      {
        i = read_c(f, buf + n, &tv);
        if(i <= 0)
          return i;
      }
      else if(buf[n] == EOT)
      {
        if(n > 2)
        {
          return
            crc_calc(buf, n - 2) == ((buf[n - 1] << 8) | buf[n - 2])
              ? n - 2 : -3;
        }

        return -2;
      }

      n++;
    }
  }

  return -4;
}
