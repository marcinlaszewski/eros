#include <ctype.h>
#include <unistd.h>

#include <boot.h>
#include <eros.h>

#define	rsp_LEN	1

#define	BUFLEN	1024
#define	MAX	11

static unsigned char hex(char c)
{
  if(c >= '0' && c <= '9')
    return c - '0';

  if(c >= 'a' && c <= 'f')
    return (c - 'a') + 10;

  return (c - 'A') + 10;
}

int boot_flash(int f, FILE * fhex)
{
  size_t n_bytes = 0;
  size_t n_lines = 0;

  for(;;)
  {
    int			c1;
    unsigned		i;
    unsigned char	text[BUFLEN] = { boot_FLASH };
    size_t		n = 1;

    for(i = 0; i < MAX; i++)
    {
      int c2;

      n_lines++;

      c1 = fgetc(fhex);
      n_bytes++;

      if(c1 == EOF)
        break;

      if(c1 != ':')
      {
        fprintf(stderr, "boot-flash: bad hex file: prefix != ':': 0x%02X\n",
          (char)c1);
        return -1;
      }


      for(;;)
      {
        c1 = fgetc(fhex);
        n_bytes++;

        if(c1 == '\r')
          continue;

        if(c1 == EOF
        || c1 == '\n')
          break;

        if(!isxdigit(c1))
        {
          fprintf(stderr, "boot-flash: not hex digit: 0x%0X\n", (unsigned)c1);
          return -2;
        }

        c2 = fgetc(fhex);
        n_bytes++;

        if(!isxdigit(c2))
        {
          fprintf(stderr, "boot-flash: not hex digit: 0x%0X\n", (unsigned)c2);
          return -2;
        }

        text[n++] = (hex(c1) << 4) + hex(c2);
      }
    }

    {
      unsigned crc = crc_calc(text, n);

      text[n++] = (crc >> 0) & 0xFF;
      text[n++] = (crc >> 8) & 0xFF;
    }

    fprintf(stderr, "boot-flash:\t%u %u\r",
      (unsigned)n_lines, (unsigned)n_bytes);
    fflush(stderr);

    {
      char	buf[BUFLEN << 1] = { SOH };
      size_t	len = 1;

      for(i = 0; i < n; i++)
      {
        switch(text[i])
        {
          case SOH:
          case DLE:
          case EOT:
            buf[len++] = DLE;
        }

        buf[len++] = text[i];
      }

      buf[len++] = EOT;

      {
        ssize_t i;

        i = (write)(f, buf, len);
        if(i < 0)
        {
          perror("write()");
          return -1;
        }

        if(i != len)
        {
          fputs("boot flash request too short.\n", stderr);
          return -2;
        }
      }
    }

    {
      int i;

      i = boot_read(f, text, sizeof(text), 5);
      if(i != rsp_LEN)
      {
        fprintf(stderr, "boot-flash: wrong response length (%u): %d\n",
          rsp_LEN, (int)i);
        return -3;
      }

      if(text[0] != boot_FLASH)
      {
        fprintf(stderr, "boot-erase: wrong response code (%u): %d\n",
          boot_ERASE, (int)i);

        return -3;
      }
    }

    if(c1 == EOF)
      break;
  }

  return 0;
}
