#include <eros.h>

int boot_load(int f, unsigned char board, char const * filename)
{
  FILE * file = fopen(filename, "rt");

  if(file == NULL)
  {
    perror(filename);
    return -1;
  }

  boot_enable(board);

  fputs("boot-erase:\t", stdout);
  fflush(stdout);

  if(boot_erase(f) < 0)
  {
    fputs("fail.\n", stdout);
    return -3;
  }

  fputs("ok\n", stdout);

  {
    int i = boot_flash(f, file);
    fclose(file);

    fputs("\nboot-flash:\t", stdout);
    if(i < 0)
    {
      fputs("fail\n", stdout);
      return -4;
    }
  }

  fputs("ok\nboot-jump:\t", stdout);
  fputs(boot_jump(f) < 0 ? "fail" : "ok", stdout);

  putchar('\n');

  boot_disable(board);

  return 0;
}
