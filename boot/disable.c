#include <unistd.h>

#include <eros.h>

void boot_disable(unsigned board)
{
  gpio_value(board_BUS[board],	0);
  usleep(10000);
  gpio_value(board_BUS[board],	1);
  gpio_value(board_GPIO[board],	1);
  usleep(100000);
}
