#include <unistd.h>

#include <boot.h>
#include <eros.h>

static unsigned char req[] =
{
  SOH, boot_JUMP, 0xA5, 0x50, EOT
};
#define	req_LEN	sizeof(req)
#define	rsp_LEN	1

int boot_jump(int f)
{
  ssize_t i;

  i = (write)(f, req, req_LEN);
  if(i < 0)
  {
    perror("write()");
    return -1;
  }

  if(i != req_LEN)
  {
    fputs("boot jump request too short.\n", stderr);
    return -2;
  }

  return 0;
}
