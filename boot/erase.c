#include <unistd.h>

#include <boot.h>
#include <eros.h>

static unsigned char req[] =
{
  SOH, boot_ERASE, 0x42, 0x20, EOT
};
#define	req_LEN	sizeof(req)
#define	rsp_LEN	1

int boot_erase(int f)
{
  ssize_t i;

  i = (write)(f, req, req_LEN);
  if(i < 0)
  {
    perror("write()");
    return -1;
  }

  if(i != req_LEN)
  {
    fputs("boot erase request too short.\n", stderr);
    return -2;
  }

  {
    unsigned char buf[1024];

    i = boot_read(f, buf, sizeof(buf), 30);
    if(i != rsp_LEN)
    {
      fprintf(stderr, "boot-erase: wrong response length (%u): %d\n",
        rsp_LEN, (int)i);
      return -3;
    }

    if(buf[0] != boot_ERASE)
    {
      fprintf(stderr, "boot-erase: wrong response code (%u): %d\n",
        boot_ERASE, (int)i);
      return -3;
    }

    return 0;
  }
}
