#include "mythic.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "eros.h"
#include "print.h"

#define	false	0

static char SetParameters (int fdUart)
{
	struct termios opts;

	if (tcgetattr(fdUart, &opts))
	{
		perror("tcgetattr");
		return -1;
	}

	 /* mode RAW, pas de mode canonique, pas d'echo */
	//BRKINT  	Signal interrupt on break.
	//ICRNL		Map CR to NL on input.
	//IGNBRK	Ignore break condition.
	//IGNCR		Ignore CR.
	//IGNPAR	Ignore characters with parity errors.
	//INLCR		Map NL to CR on input.
	//INPCK		Enable input parity check.
	//ISTRIP	Strip character.
	//IXANY		Enable any character to restart output.
	//IXOFF		Enable start/stop input control.
	//IXON		Enable start/stop output control.
	//PARMRK	Mark parity errors.
	opts.c_iflag = IGNBRK /*|IGNCR */|IXANY;
	opts.c_lflag = 0;	 /* set input mode (non-canonical, no echo,...) */
	opts.c_oflag = 0;	//The c_oflag field specifies the terminal interface's treatment of output

	opts.c_cc[VTIME]    = 0;   /* inter-character timer unused */
	opts.c_cc[VMIN]     = 1/*5*/;   /* blocking read until 5 chars received */

	if (tcsetattr(fdUart,TCSANOW,&opts))
	{
		perror("tcsetattr");
		return -1;
	}

	return 0;
}

static int serial_translate_baud(int inrate)
{
	switch(inrate)
	{
	case 0:
		return B0;
	case 300:
		return B300;
	case 1200:
		return B1200;
	case 2400:
		return B2400;
	case 4800:
		return B4800;
	case 9600:
		return B9600;
	case 19200:
		return B19200;
	case 38400:
		return B38400;
	case 57600:
		return B57600;
	case 115200:
		return B115200;
	case 230400:
		return B230400;
	case 500000:
		return B500000;

#ifdef SUPPORT_HISPEED
	case 460800:
		return B460800;
#endif
	default:
		return -1; // do custom divisor
	}
}

int serial_setbaud(int fd, int baudrate)
{
	struct termios tios;
#ifdef SUPPORT_HISPEED
	struct serial_struct ser;
#endif

	int baudratecode=serial_translate_baud(baudrate);

	if (baudratecode>0)
	{
		tcgetattr(fd, &tios);
		cfsetispeed(&tios, baudratecode);
		cfsetospeed(&tios, baudratecode);
		tcflush(fd, TCIFLUSH);
		tcsetattr(fd, TCSANOW, &tios); /// TCSANOW changes occurs now

#ifdef SUPPORT_HISPEED
		ioctl(fd, TIOCGSERIAL, &ser);

		ser.flags=(ser.flags&(~ASYNC_SPD_MASK));
		ser.custom_divisor=0;

		ioctl(fd, TIOCSSERIAL, &ser);
#endif
	}
	else
	{
#ifdef SUPPORT_HISPEED
		printf("Setting custom divisor\n");

		if (tcgetattr(fd, &tios))
			perror("tcgetattr");

		cfsetispeed(&tios, B38400);
		cfsetospeed(&tios, B38400);
		tcflush(fd, TCIFLUSH);

		if (tcsetattr(fd, TCSANOW, &tios))
			perror("tcsetattr");

		if (ioctl(fd, TIOCGSERIAL, &ser))
			perror("ioctl TIOCGSERIAL");

		ser.flags=(ser.flags&(~ASYNC_SPD_MASK)) | ASYNC_SPD_CUST;
		ser.custom_divisor=48;
		ser.custom_divisor=ser.baud_base/baudrate;
		ser.reserved_char[0]=0; // what the hell does this do?

		//      printf("baud_base %i\ndivisor %i\n", ser.baud_base,ser.custom_divisor);

		if (ioctl(fd, TIOCSSERIAL, &ser))
			perror("ioctl TIOCSSERIAL");

#endif

	}

	tcflush(fd, TCIFLUSH);

	return 0;
}

static	char const serial_dev[] = "/dev/ttyO1";

int bus_open(void)
{
  int f;

  /*info("bus_open()...");*/
  EnableTransmitionRS485(false);
    //CONFIGURE THE UART
    //The flags (defined in termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
    //  Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
    //  CSIZE:- CS5, CS6, CS7, CS8
    //  CLOCAL - Ignore modem status lines
    //  CREAD - Enable receiver
    //  IGNPAR = Ignore characters with parity errors
    //  ICRNL - Map CR to NL on input
    //  PARENB - Parity enable
    //  PARODD - Odd parity (else even)

    //OPEN THE UART
    //The flags (defined in fcntl.h):
    //  Access modes (use 1 of these):
    //      O_RDONLY - Open for reading only.
    //      O_RDWR - Open for reading and writing.
    //      O_WRONLY - Open for writing only.
    //
    //  O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
    //                                          if there is no input immediately available (instead of blocking). Likewise, write requests can also return
    //                                          immediately with a failure status if the output can't be written immediately.
    //
    //  O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the terminal device to become the controlling terminal for the process.

  f = open(serial_dev,  O_RDWR | O_NDELAY | O_NOCTTY);//non blocking read
  if(f < 0)
  {
    perror(serial_dev);
    return -1;
  }
  SetParameters(f);
  serial_setbaud(f, 115200);
  tcflush(f, TCIOFLUSH);
  return f;
}

/*
void SetEnable_pic_1 (bool val) {
    gpio_value(BITPORTEnable_pic1, val);
}
void SetEnable_pic_2 (bool val) {
    gpio_value(BITPORTEnable_pic2, val);
}
void SetEnable_pic_3 (bool val) {
    gpio_value(BITPORTEnable_pic3, val);
}
void SetEnable_pic_4 (bool val) {
    gpio_value(BITPORTEnable_pic4, val);
}
*/
