#if !defined(PRINT_H)
#define	PRINT_H

#include <stdio.h>
#include <sys/types.h>

/*
#define	BUFLEN	64
*/

typedef struct
{
  unsigned char	stx;
  unsigned char adr;
  unsigned char cmd;
  unsigned char lf;
  unsigned char	cr;
} m_resp_t;

typedef struct
{
  unsigned char	stx;
  unsigned char adr;
  unsigned char cmd;
  unsigned char len;
  unsigned char dat[0];
} m_frame_t;

#define	m_frame(d)	((m_frame_t *)(d))
#define	m_frame_HDRLEN	4

typedef struct
{
  unsigned char	code;
  char const *	str;
} code_str_t;

#define	print_TX	0
#define	print_RX	1

#if defined(__cplusplus)
extern "C" {
#endif

char const * codestr(code_str_t * t, size_t max, unsigned char code);


void		m_frame_print(FILE * f, unsigned char dir, m_frame_t * d);
#define		m_frame_print_tx(f, d)	m_frame_print(f, print_TX, d)
#define		m_frame_print_rx(f, d)	m_frame_print(f, print_RX, d)
void		m_frame_resp_print(FILE * f, m_frame_t * d);

#define		m_resp(d)	((m_resp_t *)(d))
void		m_resp_print(FILE * f, m_resp_t * d);

void print_sep(FILE * f);
void print_hex(FILE * f, unsigned char const * buf, size_t len);
void print_nl(FILE * f);

ssize_t Read(int f, void * buf, size_t len);
ssize_t Write(int f, void const * buf, size_t len);

#if defined(__cplusplus)
}
#endif

#define	read(f, b, n)	Read(f, b, n)
#define	write(f, b, n)	Write(f, b, n)

#endif

