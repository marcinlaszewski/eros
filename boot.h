#if !defined(BOOT_H)
#define	BOOT_H

#define	DLE	0x10
#define	EOT	0x04
#define	SOH	0x01

#define	boot_VER	0x1
#define	boot_ERASE	0x2
#define	boot_FLASH	0x3
#define	boot_JUMP	0x5

#endif
