#if !defined(EROS_H)
#define	EROS_H

#include <stdio.h>
#include <sys/types.h>

#include "diagdev.h"
#include "print.h"

#define	INFO	1

#if defined(INFO)
#	define	info(msg) \
      { fputs(msg, stderr); fputc('\n', stderr); fflush(stderr); }
#else
#	define	info(msg)
#endif

#define	CFG_HERMES	"/etc/xdg/Diagdev/HermesBoard.conf"

#define	CMDBUFLEN	64

#define	CR	13
#define	LF	10
#define	STX	2
#define	ACK	'A'
#define	NACK	'N'

#define	resp_ACK	1
#define	resp_NACK	(resp_ACK + 1)

#if defined(__cplusplus)
extern "C" {
#endif

int		boot(int f, unsigned board);
void		boot_disable(unsigned board);
void		boot_enable(unsigned board);
int		boot_erase(int f);
int		boot_flash(int f, FILE * fhex);
int		boot_jump(int f);
int		boot_load(int f, unsigned char board, char const * filename);
int		boot_read(int f, unsigned char * buf, size_t len,
                  unsigned timeout);
int		boot_ver(int f);

#define		m_frame_crc1(d)	(d)->dat[(d)->len]
#define		m_frame_crc2(d)	(d)->dat[(d)->len + 1]
#define		m_frame_lf(d)	(d)->dat[(d)->len + 2]
#define		m_frame_cr(d)	(d)->dat[(d)->len + 3]
unsigned	m_frame_crc_calc(m_frame_t * d);
void		m_frame_crc_set(m_frame_t * d);
unsigned	m_frame_crc(m_frame_t * d);
void		m_frame_end(m_frame_t * d);
int		m_frame_isack(m_frame_t * d);

#define		m_resp(d)	((m_resp_t *)(d))
void		m_resp_print(FILE * f, m_resp_t * d);

void		Disable_pic(char address);
int		Get_int_value(char address);
void		EnableTransmitionRS485 (bool val);

void SetEnable_pic_1_B (bool val);
void SetEnable_pic_2_B (bool val);

void EnableAcquisitionVoie1(bool val);
void EnableAcquisitionVoie2(bool val);

void MainBusReset1(bool val);
void MainBusReset2(bool val);
void MainBusReset3(bool val);
void MainBusReset4(bool val);

unsigned int	bus_crc(unsigned char const * data, long len);

int		bus_open(void);
int		bus_wait(unsigned char addr);

unsigned int	crc_calc(unsigned char const * data, long len);

int gpio_dir_in(unsigned gpio);
int Set_gpio_int_in(int gpio);
int gpio_export(unsigned gpio);
/*
int gpio_unexport(unsigned gpio);
*/
int gpio_dir_out(unsigned gpio);
int gpio_value(unsigned gpio, unsigned value);

void Enable_pic(char address);

void SetEnable_pic_1 (bool val);
void SetEnable_pic_2 (bool val);
void SetEnable_pic_3 (bool val);
void SetEnable_pic_4 (bool val);

/*
void MainBusReset1(bool val);
void MainBusReset2 (bool val);
*/


char const * board_name(unsigned char addr);


int answer(int f, unsigned char addr, unsigned char data[64]);
int answer_check(int f, long sec, long usec);

int m120_carriage(int f, unsigned char pos);
int m120_needle(int f, unsigned char n_cmd);
int m120_switch(int f, unsigned char board, unsigned char cmd);

char	const * config(char const * name);
int	config_read(char const * name);
int	config_int(char const * name, int default_value);

int	atl_rev(int f, unsigned char rev);
int	cmd_temp(int f);
int	device_detect(void);
int	gpio_init(void);
int	init_boards(int f, unsigned char type, unsigned char rev);
void	reset_all(void);
int	wbc_init(int f);
int	send_arg0(int f, unsigned char board, unsigned char cmd);
int	send_arg1(int f, unsigned char board, unsigned char cmd,
            unsigned char data);
int	send_arg2(int f, unsigned char board, unsigned char cmd,
            unsigned char arg1, unsigned char arg2);
int	send_cmd(int f, unsigned char addr, unsigned char cmd,
          unsigned char * data, unsigned char buffer[CMDBUFLEN]);
/*
int	send_cmd2(int f, unsigned char addr, unsigned char cmd,
          unsigned char * data, unsigned char buf[BUFLEN]);
*/
int	send_dilpos(int f, unsigned char cmd, char const * name);
int	send_int(int f, unsigned char board, unsigned char cmd, int value);
int	send_int_unit(int f, unsigned char board, unsigned char cmd,
            int value, char unit);
int	send_int_unit_app(int f, unsigned char device,
            unsigned char cmd120, unsigned char cmd80, int value, char unit);
int	send_temp(int f);
int	spi_init(char const * file);

int	Select(int n, fd_set * fr, fd_set * fw, fd_set * fe,
          struct timeval * tv);

int	TCDRAIN(int f);

#if defined(__cplusplus)
}
#endif

#define	TO_ATL_REV	(100000)
#define	TO_WBC		(2000)
#define	TO_WBC_IO	(100)
#define	TO_WBC_RS	(1000)

#define	TO_BUS_IO	(100)
#define	TO_GPIO_RESET	(10000)
#define	TO_RESET	(500000)
#define	TO_REP_OFF	(2000)

/*#define TO_REP_ENABLE	(100)*/
#define	TO_REP_ENABLE	(1000)
#define	TO_REP_DISABLE	(2000)

#define	DEVICE_TYPE	MYTHIC120
#define	DEVICE_REV	REVISION_C

#define	board_RBC	0
#define	board_WBC	(board_RBC	+ 1)
#define	board_DIL	(board_WBC	+ 1)
#define	board_ATL	(board_DIL	+ 1)
#define	board_RBC_ACQ	(board_ATL	+ 1)
#define	board_WBC_ACQ	(board_RBC_ACQ	+ 1)
#define	board_MAX	(board_WBC_ACQ	+ 1)
#define	board_MAIN_MAX	(board_ATL	+ 1)

#define	board_IS_MAIN(n)	((n) >= board_RBC && (n) < board_MAIN_MAX)
#define	board_IS_ACQ(n)		((n) == board_RBC_ACQ || (n) == board_WBC_ACQ)
#define	board_ADDR(idx)		((idx) + '1')
#define	board_INDEX(adr)	((adr) - '1')

#define	board_FD_MAX	board_MAIN_MAX

#define	board_set_DISABLE	1
#define	board_set_ENABLE	0

void board_set(unsigned char addr, unsigned char state);
#define	board_disable(addr)	board_set(addr, board_set_DISABLE)
#define	board_enable(addr)	board_set(addr, board_set_ENABLE)

extern	int board_FD[board_FD_MAX];
extern	unsigned board_BUS[];
extern	unsigned board_GPIO[];

typedef struct
{
  char * name;
  char * value;
} config_t;

extern	size_t		config_MAX;
extern	config_t *	config_DATA;

/* Czas (us) na pojawienie sie gotowosci karty po wyslaniu 'enable' */
#define	board_TM_ENABLE	1000

#define	SPI_0	"/dev/spidev1.0"
#define	SPI_1	"/dev/spidev2.0"

extern	int spi0;
extern	int spi1;


#endif

