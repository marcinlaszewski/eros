CFLAGS	+= -Wall
CFLAGS	+= -O
CFLAGS	+= -I.

CROSS	= arm-cortexa8-linux-gnueabihf-
CC	= $(CROSS)gcc
GDB	= $(CROSS)gdb
STRIP	= $(CROSS)strip

NAME	= eros

OBJS	= \
		board/bus.o \
		board/gpio.o \
		boot/disable.o \
		boot/enable.o \
		boot/erase.o \
		boot/flash.o \
		boot/jump.o \
		boot/load.o \
		boot/read.o \
		boot/ver.o \
		bus.o \
		crc/calc.o \
		gpio.o \
		main.o \

OUT	= $(NAME)

$(OUT):	$(OBJS)
	$(CC) -o $(OUT) $(LDFLAGS) $(OBJS) $(IBS)

clean::
	$(RM) $(OBJS)
