int gpio_open_read(char * path, char const * type)
{
	int		ff;
  char	path[MAX_BUF];

  strcpy(path, SYSFS_GPIO_DIR));
  strcat(path, "/");
  strcat(type);

  f = open(path, O_RDONLY);

  if(f < 0)
    perror(path);

  return f;
}

int gpio_wait()
{
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	/*printf("gpio_get_value(%u)\t%s\n", gpio, buf);*/

	fd = open(buf, O_RDONLY);
	if (fd < 0)
	{
		perror("gpio/get-value");
		return fd;
	}

	{
	  int i;

    i = read(fd, buf, sizeof(buf));
    if(i < 0)
      perror("read");
  
    if(i > 0)
    {
      *value = (*buf == '0') ? 0 : 1;
      i = 1;
    }
    else
      i = 0;

	  close(fd);
	  return i;
  }
}
