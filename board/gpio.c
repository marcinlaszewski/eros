#include "eros.h"
#include "mythic.h"

unsigned board_GPIO[] =
{
  BITPORTEnable_pic1,
  BITPORTEnable_pic2,
  BITPORTEnable_pic3,
  BITPORTEnable_pic4,
  BITPORTEnable_pic1_B,
  BITPORTEnable_pic2_B,
};
