#include <stdio.h>
#include <string.h>

#include "mythic.h"
#include "eros.h"

int board_FD[board_FD_MAX] = { -1, -1, -1, -1 };

#define	DEVICE_TYPE	MYTHIC120
#define	DEVICE_REV	REVISION_C

#define	table_size(t)	(sizeof(t) / sizeof(*(t)))

static signed char board_nick2id(char const * nick)
{
  char const *	name[] = { "rbc", "wbc", "dil", "atl", "acq-rbc", "acq-wbc" };
  signed char	b;

  for(b = 0; b < table_size(name); b++)
    if(!strcmp(nick, name[b]))
      return b;

  fputs("Unknown board: ",	stderr);
  fputs(nick,			stderr);
  fputc('\n',			stderr);

  for(b = 0; b < table_size(name); b++)
  {
    fputc('\t',		stderr);
    fputs(name[b],	stderr);
    fputc('\n',		stderr);
  }

  return -1;
}

int main(int argc, char const * argv[])
{
  int f;
  int arg = 1;

  if(argc < 2)
  {
    fputs(argv[0], stderr);
    fputs("\n"
          "\t--boot-erase <board>\n"
          "\t--boot-load <board> <file.hex>\n"
          "\t--boot-ver <board>\n", stderr);
    return 1;
  }

  setlinebuf(stdout);
  setlinebuf(stderr);

  gpio_init();

  f = bus_open();
  if(f < 0)
  {
    perror("bus_open");
    return 2;
  }

  board_FD[board_RBC] = Set_gpio_int_in(IT_BOARD_1);
  board_FD[board_WBC] = Set_gpio_int_in(IT_BOARD_2);
  board_FD[board_DIL] = Set_gpio_int_in(IT_BOARD_3);
  board_FD[board_ATL] = Set_gpio_int_in(IT_BOARD_4);

  while(arg < argc)
  {
    if(!strcmp(argv[arg], "--boot-erase"))
    {
      signed char b;

      if(argc - arg < 2)
      {
        fputs("--boot-erase <board>\n", stderr);
        return 1;
      }

      arg++;

      b = board_nick2id(argv[arg]);
      if(b < 0)
        return 2;

      fputs("boot-board:\t",	stdout);
      fputs(argv[arg],		stdout);
      putchar('\n');

      boot_enable(b);

      fputs("boot-erase:\t",	stdout);
      fflush(stdout);
      puts(boot_erase(f) < 0 ? "fail" : "ok");

      boot_disable(b);

      putchar('\n');
    }
    else if(!strcmp(argv[arg], "--boot-load"))
    {
      signed char b;

      if(argc - arg < 3)
      {
        fputs("--boot-load <board> <file.hex>\n", stderr);
        return 1;
      }

      arg++;

      b = board_nick2id(argv[arg]);
      if(b < 0)
        return 2;

      fputs("boot-board:\t",	stdout);
      fputs(argv[arg],		stdout);

      arg++;

      fputs("\nboot-file:\t",	stdout);
      fputs(argv[arg],		stdout);
      putchar('\n');

      boot_load(f, b, argv[arg]);
      putchar('\n');
    }
    else if(!strcmp(argv[arg], "--boot-ver"))
    {
      signed char b;

      if(argc - arg < 2)
      {
        fputs("--boot-ver <board>\n", stderr);
        return 1;
      }

      arg++;

      b = board_nick2id(argv[arg]);
      if(b < 0)
        return 2;

      fputs("boot-board:\t",	stdout);
      fputs(argv[arg],		stdout);
      putchar('\n');

      boot_enable(b);

      {
        int a = boot_ver(f);

        if(a >= 0)
          printf("boot-ver:\t%04X\n", a);
      }

      boot_disable(b);
      putchar('\n');
    }
    else
    {
      fputs(argv[0], stderr);
      fputs(" Unknown parameter: ", stderr);
      fputs(argv[arg], stderr);
      fputc('\n', stderr);
      return 1;
    }

    arg++;
  }

  return 0;
}
