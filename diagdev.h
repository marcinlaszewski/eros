#if !defined(DIGDEV_H)
#define	DIAGDEV_H

#define	true	1
#define	false	0
#define	bool	unsigned

enum DeviceType
{
  MYTHIC120,
  MYTHIC80,
};

enum DeviceRevision
{
  REVISION_B,
  REVISION_C,
};

#endif
