#include <errno.h>

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "mythic.h"
#include "eros.h"

#define SYSFS_GPIO_DIR	"/sys/class/gpio"
#define MAX_BUF					64

/*
int gpio_fd_IT_BOARD_1;
int gpio_fd_IT_BOARD_2;
int gpio_fd_IT_BOARD_3;
int gpio_fd_IT_BOARD_4;
*/

#undef	read
#undef	write

char const path_EXPORT[] = "/sys/class/gpio/export";

int gpio_export(unsigned gpio)
{
	int fd;

	fd = open(path_EXPORT, O_WRONLY);
	if(fd < 0)
	{
		perror(path_EXPORT);
		return -1;
	}

	{
	  int len, i;
  	char buf[MAX_BUF];

  	len = snprintf(buf, sizeof(buf), "%u", gpio);
	
  	i = write(fd, buf, len);
  	if(i < 0)
  	{
  	  if(errno == EBUSY)
  	    i = 0;
      else
      {
	      sprintf(buf, "gpio_export(%u) = %d", gpio, errno);
	      perror(buf);
      }
    }

  	close(fd);
  	return i < 0 ? -2 : 0;
	}
}

int gpio_fd_open(unsigned int gpio)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_RDONLY | O_NONBLOCK);
	if(fd < 0)
    perror(buf);

	return fd;
}
int gpio_set_edge(unsigned int gpio, const char *edge)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);
	fd = open(buf, O_WRONLY);
	if(fd < 0)
	{
		perror(buf);
		return fd;
	}

	{
	  int i;
	  
	  i = write(fd, edge, strlen(edge));
	  if(i < 0)
	    perror("write");
  	close(fd);
  	return i;
  }
}
int gpio_set_dir(unsigned int gpio, unsigned int out_flag)
{
	int fd;
	char buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);

	fd = open(buf, O_WRONLY);
	if(fd < 0)
	{
		perror("gpio/direction");
		return fd;
	}

	{
	  int i;

  	if(out_flag)
	  	i = write(fd, "out", 4);
    else
		  i = write(fd, "in", 3);
    if(i < 0)
      perror("write");
  	close(fd);
  	return i;
  }
}

int gpio_dir(unsigned gpio, unsigned dir)
{
	int fd;
	char buf[MAX_BUF];

	 snprintf(buf, sizeof(buf), "/sys/class/gpio/gpio%d/direction", gpio);

	fd = open(buf, O_WRONLY);
	if(fd < 0)
	{
		perror("gpio/direction");
		return fd;
	}

	{
	  int i;

  	if(dir == GPIO_DIR_OUT)
	  	i = write(fd, "out", 4);
    else
		  i = write(fd, "in", 3);
    if(i < 0)
      perror("write");
    close(fd);
    return i < 0 ? -1 : 0;
  }
}

int gpio_dir_out(unsigned gpio)
{
	return gpio_dir(gpio, GPIO_DIR_OUT);
}

int Set_gpio_int_in(int gpio )
{
	gpio_export(gpio);
	gpio_set_dir(gpio, 0);
	gpio_set_edge(gpio, "rising");//rising/falling/both
	return gpio_fd_open(gpio);
}

int gpio_value(unsigned gpio, unsigned value)
{
	int		fd;
	char	buf[MAX_BUF];

	/*printf("gpio_value(%u, %u)\n", gpio, value);*/

	snprintf(buf, sizeof(buf), "/sys/class/gpio/gpio%d/value", gpio);

	fd = open(buf, O_WRONLY);

	if (fd < 0) {
		perror("gpio/value");
		printf("gpio :%d  \n",gpio);
		return fd;
	}

	{
	  int i;

  	if(value)
		  i = write(fd, "1", 2);
    else
		  i = write(fd, "0", 2);
    if(i < 0)
      perror("write");
    close(fd);
    return i < 0 ? -1 : 0;
  }
}

void EnableAcquisitionVoie1(bool val) {
	gpio_value(DATA_BUS_INT0, val);
}
void EnableAcquisitionVoie2(bool val) {
	gpio_value(DATA_BUS_INT1, val);
}

int gpio_get_value(unsigned int gpio, unsigned int *value)
{
	int		fd;
	char	buf[MAX_BUF];

	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
	/*printf("gpio_get_value(%u)\t%s\n", gpio, buf);*/

	fd = open(buf, O_RDONLY);
	if (fd < 0)
	{
		perror("gpio/get-value");
		return fd;
	}

	{
	  int i;

    i = read(fd, buf, sizeof(buf));
    if(i < 0)
      perror("read");
  
    if(i > 0)
    {
      *value = (*buf == '0') ? 0 : 1;
      i = 1;
    }
    else
      i = 0;

	  close(fd);
	  return i;
  }
}

int Get_int_value(char address)
{
  static unsigned int board[] =
  {
    IT_BOARD_1,
    IT_BOARD_2,
    IT_BOARD_3,
    IT_BOARD_3
  };

	unsigned int value;

  gpio_get_value(board[address - '1'], &value);
	return value;
}

#define	true	1
#define	false	0
#define	bool	unsigned

void SetEnable_pic_1 (bool val)
{
	gpio_value(BITPORTEnable_pic1, val);
}

void SetEnable_pic_2 (bool val)
{
	gpio_value(BITPORTEnable_pic2, val);
}

void SetEnable_pic_3 (bool val)
{
	gpio_value(BITPORTEnable_pic3, val);
}

void SetEnable_pic_4 (bool val)
{
	gpio_value(BITPORTEnable_pic4, val);
}

void EnableTransmitionRS485 (bool val)
{
  /*fprintf("RS485: %s\n", val ? "disable" : "enable");*/
	gpio_value(ENABLE_TRANSMISSION_RS485, val);
}


void MainBusReset1(bool val) {
	gpio_value(MAINBUSRESET_CARTE1, val);
}
void MainBusReset2 (bool val) {
	gpio_value(MAINBUSRESET_CARTE2, val);
}
void MainBusReset1_B(bool val) {
    gpio_value(MAINBUSRESET_CARTE1, val);
}
void MainBusReset2_B(bool val) {
    gpio_value(MAINBUSRESET_CARTE2, val);
}
void MainBusReset3 (bool val) {
	gpio_value(MAINBUSRESET_CARTE3, val);
}
void MainBusReset4 (bool val) {
	gpio_value(MAINBUSRESET_CARTE4, val);
}

void SetEnable_pic_1_B (bool val) {
    gpio_value(BITPORTEnable_pic1_B, val);
}
void SetEnable_pic_2_B (bool val) {
    gpio_value(BITPORTEnable_pic2_B, val);
}

static int gpio_export_in(unsigned gpio)
{
  if(gpio_export(gpio) < 0)
    return -1;
  if(gpio_dir_in(gpio) < 0)
    return -2;
  return 0;
}

static int gpio_export_out(unsigned gpio)
{
  if(gpio_export(gpio) < 0)
    return -1;
  if(gpio_dir_out(gpio) < 0)
    return -2;
  return 0;
}

int gpio_init(void)
{
  /*info("gpio_init()...");*/

  gpio_export_out(BITPORTEnable_pic1);
  gpio_export_out(BITPORTEnable_pic2);
  gpio_export_out(BITPORTEnable_pic3);
  gpio_export_out(BITPORTEnable_pic4);

  gpio_export_out(BITPORTEnable_pic1_B);
  gpio_export_out(BITPORTEnable_pic2_B);

  gpio_export_out(MAINBUSRESET_CARTE1);
  gpio_export_out(MAINBUSRESET_CARTE2);
  gpio_export_out(MAINBUSRESET_CARTE3);
  gpio_export_out(MAINBUSRESET_CARTE4);

  gpio_export_out(ENABLE_BUFFER);
  gpio_export_out(ENABLE_TRANSMISSION_RS485);

  gpio_export_out(DATA_BUS_INT0);
  gpio_export_out(DATA_BUS_INT1);

  gpio_export_in(IT_BOARD_1);
  gpio_export_in(IT_BOARD_2);
  gpio_export_in(IT_BOARD_3);
  gpio_export_in(IT_BOARD_4);

/*
  gpio_fd_IT_BOARD_1 = Set_gpio_int_in(IT_BOARD_1);
  gpio_fd_IT_BOARD_2 = Set_gpio_int_in(IT_BOARD_2);
  gpio_fd_IT_BOARD_3 = Set_gpio_int_in(IT_BOARD_3);
  gpio_fd_IT_BOARD_4 = Set_gpio_int_in(IT_BOARD_4);
*/

//	gpio_fd_DATA_BUS_INT0 = Set_gpio_int_in(DATA_BUS_INT0);
//	gpio_fd_DATA_BUS_INT1 = Set_gpio_int_in(DATA_BUS_INT1);

  SetEnable_pic_1(true);
  SetEnable_pic_2(true);
  SetEnable_pic_3(true);
  SetEnable_pic_4(true);
  SetEnable_pic_1_B(true);
  SetEnable_pic_2_B(true);

  MainBusReset1(false);
  MainBusReset2(false);
  MainBusReset3(false);
  MainBusReset4(false);
  usleep(TO_GPIO_RESET);

  MainBusReset1(true);
  MainBusReset2(true);
  MainBusReset3(true);
  MainBusReset4(true);
  usleep(TO_GPIO_RESET);

	EnableAcquisitionVoie1(false);
	EnableAcquisitionVoie2(false);

	return 0;
}

int gpio_dir_in(unsigned gpio)
{
	return gpio_dir(gpio, GPIO_DIR_IN);
}
