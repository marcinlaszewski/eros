#if !defined(MYTHIC_H)
#define	MYTHIC_H

#define	false	0
#define	true	1

/*
GPIO aktywacji kart
*/
#define BITPORTEnable_pic1	(32 + 16)	/* GPIO 1_16	*/
#define BITPORTEnable_pic2	(32 + 17)	/* GPIO 1_17	*/
#define BITPORTEnable_pic3	(32 + 18)	/* GPIO 1_18	*/
#define BITPORTEnable_pic4	(32 + 19)	/* GPIO 1_19	*/

#define BITPORTEnable_pic1_B	(32*3 + 15)		// GPIO 3_15
#define BITPORTEnable_pic2_B	(32*1 + 23)		// GPIO 1_23
/*---------------*/
      
/*
GPIO przewania od karty
*/
#define IT_BOARD_1		(32*1 + 30)	/* GPIO 1_30	*/
#define IT_BOARD_2		(32*1 + 25)	/* GPIO 1_24	*/
#define IT_BOARD_3		(32*1 + 26)	/* GPIO 1_25	*/
#define IT_BOARD_4		(32*1 + 27)	/* GPIO 1_26	*/
/*---------------*/

#define MAINBUSRESET_CARTE1		(32*3 + 19)		// GPIO 3_19
#define MAINBUSRESET_CARTE2		(32*3 + 20)		// GPIO 3_20
#define MAINBUSRESET_CARTE3		(32*3 + 21)		// GPIO 3_21
#define MAINBUSRESET_CARTE4		(32*3 + 14)		// GPIO 3_14

#define ENABLE_BUFFER			(32*3 + 7)	// GPIO 3_7
#define ENABLE_TRANSMISSION_RS485	(32*3 + 8)	// GPIO 3_8

#define DATA_BUS_INT0			(32*1 + 20)		// GPIO 1_20
#define DATA_BUS_INT1			(32*1 + 21)		// GPIO 1_21

#define GPIO_DIR_IN	0
#define GPIO_DIR_OUT	1

#define	DILUTOR_BOARD	'3'
#define	RBC_MAIN_BOARD	'1'
#define	RBC_ACQ_BOARD	'5'
#define	WBC_MAIN_BOARD	'2'
#define	WBC_ACQ_BOARD	'6'
#define	ATL_BOARD	'4'

#define	CALIBRATION_FILE	"/etc/pointercal"

#define	SWITCH_MAX	13

#include "mythic/defaultValues.h"
#include "mythic/ListCommandesSchedulerHermes.h"
#include "mythic/motorsposition.h"

#define SPI_IOC_RD_TIMEOUT              _IOR(SPI_IOC_MAGIC, 5, __u32)
#define SPI_IOC_WR_TIMEOUT              _IOW(SPI_IOC_MAGIC, 5, __u32)

#endif

